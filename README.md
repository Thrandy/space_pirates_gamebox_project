# Space_Pirates

Developed with Unreal Engine 4

Test task for GameBox

***ENG***

**Space_Pirates** - a first-person space shooter.
You need to destroy ships to score points, as well as collect useful loot.

Loot comes in different colors:

* red loot - reduces the recharge rate by 0.5 seconds (max. 0.5 shoot per sec)
* orange loot - increases the speed of the ship
* yellow loot - increases the maximum number of shots in different directions
per shot (up to 5 rounds)

There are 4 types of enemies in the game:

* normal (green) - does not pose any threat
* advanced (blue) - can shoot in the direction where it flies
* thunderbird (purple) - shoots three projectiles in different directions
* the boss (purple with shield) - the most dangerous enemy is the same as 'thunderbird', 
but it has a shield, the shield becomes harder to break every time!

As you gain points, the speed of your enemies increases!

Control:

* the cursor - the direction of the shot
* LMB (pressed) - the ship is flying to the specified point
* RMB (click) - single shot
* ESC / Q - calls the pause menu


***RUS***

**Space_Pirates** - спейс-шутер от первого лица.

Вам нужно уничтожать корабли, чтобы набрать очки, а также собирать полезный лут.

Лут бывает разных цветов:
* красный уменьшает скорость перезарядки на 0.5 секунды вплоть (макс. 0.5 шт/сек)
* оранжевый увеличивает скорость корабля
* желтый увеличивает максимальное количество выстрелов в разные стороны 
за один выстрел (вплоть до 5 снарядов)

В игре есть 4 вида врагов:
* обычный (зеленый) - не представляет никакой угрозы
* продвинутый (синий) - может стрелять в сторону, куда и летит
* буревестник (фиолетовый) - стреляет тремя снарядами в разные стороны
* босс (фиолетовый с щитом) - самый опасный враг, тоже что и 'буревестник', но имеет щит
щит с каждым разом становится все сложнее пробить!

По мере набора очков скорость врагов увеличивается!

Управление:
* Курсор - направление выстрела
* ЛКМ (удержание) - корабль летит в указанную точку
* ПКМ (нажатие) - одиночный выстрел
* ESC / Q - вызов меню-паузы
